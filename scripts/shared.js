let backdrop = document.querySelector('.backdrop')
let modal = document.querySelector('.modal')
let selectPlanBtn = document.querySelectorAll('.plan button')
let closeBtn = document.querySelector('.negative')

for (let i = 0; i < selectPlanBtn.length; i++) {
    selectPlanBtn[i].addEventListener('click', () => {
        modal.style.display = 'block'
        backdrop.style.display = 'block'
    })
}

closeBtn.addEventListener('click',close)
backdrop.addEventListener('click',close)

function close () {
    modal.style.display = 'none'
    backdrop.style.display = 'none'
}

console.log(modal)
console.log(backdrop)